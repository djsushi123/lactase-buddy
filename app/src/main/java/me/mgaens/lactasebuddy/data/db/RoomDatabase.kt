package me.mgaens.lactasebuddy.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import me.mgaens.lactasebuddy.lactasecontainers.data.db.dao.LactaseContainerDao
import me.mgaens.lactasebuddy.lactasecontainers.data.db.entity.LactaseContainerEntity


@Database(entities = [LactaseContainerEntity::class], version = 1)
@TypeConverters(RoomTypeConverters::class)
abstract class LactaseDatabase : RoomDatabase() {

    abstract fun lactaseContainerDao(): LactaseContainerDao
}
