package me.mgaens.lactasebuddy.lactasecontainers.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal
import java.time.Instant
import java.util.Date

@Entity
data class LactaseContainerEntity(
    @PrimaryKey val code: String,
    val pillsLeft: Int,

    /**
     * The amount of IU (international units) per single pill
     */
    val lactaseIUPerPill: Int,
    val datePurchased: Date,

    /**
     * The price doesn't contain a currency and that is deliberate, because the currency will be a
     * global setting for the user. There will be a single currency for all purchases, because
     * converting between currencies is out of the scope of this app.
     */
    val price: BigDecimal,
    val expiryDate: Date?,
    val expiryDateHasDay: Boolean
)
