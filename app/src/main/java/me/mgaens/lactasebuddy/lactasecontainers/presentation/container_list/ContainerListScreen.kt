package me.mgaens.lactasebuddy.lactasecontainers.presentation.container_list

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container.NewContainerViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun LactaseContainersScreen(viewModel: NewContainerViewModel = koinViewModel()) {
    Scaffold(
        floatingActionButton = {
            FAB(
                onClick = {  }
            )
        }
    ) {

    }
}

@Composable
private fun FAB(onClick: () -> Unit) {
    ExtendedFloatingActionButton(
        text = {
            Text(text = "New")
        },
        icon = {
            Icon(imageVector = Icons.Default.Add, contentDescription = "New Lactase Container")
        },
        onClick = onClick
    )
}