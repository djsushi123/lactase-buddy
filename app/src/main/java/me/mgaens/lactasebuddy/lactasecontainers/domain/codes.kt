package me.mgaens.lactasebuddy.lactasecontainers.domain

private const val CHARACTER_SET = "ACDEFGHJKLMNPQRTUVWXY34679"

fun generateCode(): String = buildString {
    repeat(3) {
        append(CHARACTER_SET.random())
    }
}