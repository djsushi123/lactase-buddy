package me.mgaens.lactasebuddy.lactasecontainers.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import me.mgaens.lactasebuddy.lactasecontainers.data.db.entity.LactaseContainerEntity

@Dao
interface LactaseContainerDao {

    @Insert
    suspend fun insertLactaseContainer(lactaseContainer: LactaseContainerEntity): Long
}