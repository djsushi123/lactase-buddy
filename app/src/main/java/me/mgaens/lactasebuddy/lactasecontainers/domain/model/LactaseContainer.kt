package me.mgaens.lactasebuddy.lactasecontainers.domain.model

data class LactaseContainer(
    val code: String
)