package me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container

import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import me.mgaens.lactasebuddy.R
import me.mgaens.lactasebuddy.ui.composable.DateChooserDialog
import me.mgaens.lactasebuddy.ui.composable.InfoDialog
import me.mgaens.lactasebuddy.ui.composable.InfoTextField
import me.mgaens.lactasebuddy.ui.composable.OutlinedDateField
import me.mgaens.lactasebuddy.ui.spacing
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NewLactaseContainerScreen(
    navController: NavHostController,
    viewModel: NewContainerViewModel = koinViewModel()
) {
    val state by viewModel.state.collectAsState()

    var showLactaseIUInfoDialog by rememberSaveable { mutableStateOf(false) }
    var showPillsLeftInfoDialog by rememberSaveable { mutableStateOf(false) }
    var showPurchasedDialog by rememberSaveable { mutableStateOf(false) }
    var showExpiryDialog by rememberSaveable { mutableStateOf(false) }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "New Container") },
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Back"
                        )
                    }
                },
                actions = {
                    IconButton(onClick = { navController.navigateUp() }) {
                        Icon(imageVector = Icons.Default.Check, contentDescription = "Save")
                    }
                }
            )
        }
    ) { scaffoldPadding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(scaffoldPadding)
        ) {
            CodeTextField(
                value = state.code,
                onValueChange = {},
                onRefresh = { viewModel.onEvent(NewContainerEvent.RefreshCode) }
            )
            Row(modifier = Modifier.fillMaxWidth()) {
                InfoTextField(
                    value = state.pillsLeft?.toString() ?: "",
                    onValueChange = {
                        viewModel.onEvent(NewContainerEvent.ChangePillsLeft(amount = it))
                    },
                    labelText = stringResource(R.string.pills_left),
                    isError = false,
                    onInfoOpen = { showPillsLeftInfoDialog = true },
                    modifier = Modifier
                        .weight(1f)
                        .padding(MaterialTheme.spacing.small)
                )
                InfoTextField(
                    value = state.lactaseIU?.toString() ?: "",
                    onValueChange = {
                        viewModel.onEvent(NewContainerEvent.ChangeLactaseIU(amount = it))
                    },
                    labelText = stringResource(R.string.lactase_iu),
                    isError = false,
                    onInfoOpen = { showLactaseIUInfoDialog = true },
                    modifier = Modifier
                        .weight(1f)
                        .padding(MaterialTheme.spacing.small)
                )
            }
            Row(modifier = Modifier.fillMaxWidth()) {
                PurchasedDateField(
                    value = state.purchasedOn,
                    onClick = { showPurchasedDialog = true }
                )
                ExpiryDateField(
                    value = state.expiryDate,
                    onClick = { showExpiryDialog = true }
                )
            }
        }
    }

    Dialogs(
        showLactaseIUInfoDialog = showLactaseIUInfoDialog,
        onLactaseIUInfoDialogDismiss = {
            showLactaseIUInfoDialog = false
        },
        showPillsLeftInfoDialog = showPillsLeftInfoDialog,
        onPillsLeftDialogDismiss = {
            showPillsLeftInfoDialog = false
        },
        showPurchasedDialog = showPurchasedDialog,
        onPurchasedDialogDismiss = {
            showPurchasedDialog = false
        },
        onPurchasedDialogSave = {
            showPurchasedDialog = false
            viewModel.onEvent(NewContainerEvent.ChangePurchaseDate(it))
        },
        purchasedDate = state.purchasedOn,
        showExpiryDialog = showExpiryDialog,
        onExpiryDialogDismiss = {
            showExpiryDialog = false
        },
        onExpiryDialogSave = {
            showExpiryDialog = false
            viewModel.onEvent(NewContainerEvent.ChangeExpiryDate(it))
        },
        expiryDate = state.expiryDate
    )
}

@Composable
fun Dialogs(
    showLactaseIUInfoDialog: Boolean,
    onLactaseIUInfoDialogDismiss: () -> Unit,
    showPillsLeftInfoDialog: Boolean,
    onPillsLeftDialogDismiss: () -> Unit,
    showPurchasedDialog: Boolean,
    onPurchasedDialogDismiss: () -> Unit,
    onPurchasedDialogSave: (Long) -> Unit,
    purchasedDate: Long,
    showExpiryDialog: Boolean,
    onExpiryDialogDismiss: () -> Unit,
    onExpiryDialogSave: (Long) -> Unit,
    expiryDate: Long
) {
    LactaseIUInfoDialog(
        show = showLactaseIUInfoDialog,
        onDismiss = onLactaseIUInfoDialogDismiss
    )
    PillsLeftInfoDialog(
        show = showPillsLeftInfoDialog,
        onDismiss = onPillsLeftDialogDismiss
    )
    DateChooserDialog(
        show = showPurchasedDialog,
        onDismiss = onPurchasedDialogDismiss,
        onSave = onPurchasedDialogSave,
        initialDate = purchasedDate
    )
    DateChooserDialog(
        show = showExpiryDialog,
        onDismiss = onExpiryDialogDismiss,
        onSave = onExpiryDialogSave,
        initialDate = expiryDate
    )
}

@Composable
fun CodeTextField(
    value: String,
    onValueChange: (String) -> Unit,
    onRefresh: () -> Unit
) {
    var refreshIconRotation by remember { mutableFloatStateOf(0f) }
    val animatedRefreshIconRotation by animateFloatAsState(
        targetValue = refreshIconRotation,
        animationSpec = TweenSpec(),
        label = "Refresh Icon Rotation"
    )

    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        readOnly = true,
        singleLine = true,
        label = { Text(text = stringResource(R.string.code)) },
        keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Characters),
        trailingIcon = {
            IconButton(
                onClick = {
                    onRefresh()
                    refreshIconRotation += 360f
                    println(animatedRefreshIconRotation)
                }
            ) {
                Icon(
                    painter = painterResource(R.drawable.baseline_autorenew_24),
                    contentDescription = "Reload",
                    modifier = Modifier.rotate(animatedRefreshIconRotation)
                )
            }
        },
        textStyle = TextStyle(
            fontFamily = FontFamily.Monospace,
            letterSpacing = 1.sp,
            fontSize = 24.sp
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(MaterialTheme.spacing.small)
    )
}

@Composable
fun LactaseIUInfoDialog(show: Boolean, onDismiss: () -> Unit) {
    InfoDialog(
        show = show,
        onDismiss = onDismiss,
        text = stringResource(R.string.lactase_iu_info)
    )
}

@Composable
fun PillsLeftInfoDialog(show: Boolean, onDismiss: () -> Unit) {
    InfoDialog(
        show = show,
        onDismiss = onDismiss,
        text = "The amount of pills you have left in the lactase container."
    )
}

@Composable
fun RowScope.PurchasedDateField(value: Long, onClick: () -> Unit) {
    OutlinedDateField(
        value = value,
        onClick = onClick,
        label = stringResource(R.string.purchased_on),
        modifier = Modifier
            .weight(1f)
            .padding(MaterialTheme.spacing.small)
    )
}

@Composable
fun RowScope.ExpiryDateField(value: Long, onClick: () -> Unit) {
    OutlinedDateField(
        value = value,
        onClick = onClick,
        label = stringResource(R.string.expires_on),
        modifier = Modifier
            .weight(1f)
            .padding(MaterialTheme.spacing.small)
    )
}
