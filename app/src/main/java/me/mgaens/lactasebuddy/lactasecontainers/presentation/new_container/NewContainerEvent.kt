package me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container

sealed class NewContainerEvent {
    data object RefreshCode : NewContainerEvent()
    data class ChangeLactaseIU(val amount: String) : NewContainerEvent()
    class ChangePurchaseDate(val date: Long) : NewContainerEvent()
    class ChangeExpiryDate(val date: Long) : NewContainerEvent()
    class ChangePillsLeft(val amount: String) : NewContainerEvent()
}
