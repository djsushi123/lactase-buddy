package me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container

data class NewContainerState(
    val code: String = "",
    val lactaseIU: Int? = null,
    val pillsLeft: Int? = null,
    val purchasedOn: Long = 0L,
    val price: Int? = null,
    val expiryDate: Long = 0L,
    val expiryDateHasDay: Boolean = true
)
