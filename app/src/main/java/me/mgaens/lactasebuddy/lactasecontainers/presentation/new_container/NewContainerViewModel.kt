package me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import me.mgaens.lactasebuddy.lactasecontainers.domain.generateCode
import java.time.Duration
import java.time.Instant
import java.util.Date

class NewContainerViewModel : ViewModel() {

    private var _state = MutableStateFlow(NewContainerState())
    val state: StateFlow<NewContainerState> = _state.asStateFlow()

    init {
        val currentDate = Date().time
        val expiryDate =
            Instant.ofEpochMilli(currentDate).plus(Duration.ofDays(365)).epochSecond * 1000
        _state.update { currentState ->
            currentState.copy(purchasedOn = currentDate, expiryDate = expiryDate)
        }
        refreshCode()
    }

    fun onEvent(event: NewContainerEvent) {
        when (event) {
            is NewContainerEvent.RefreshCode -> {
                println("refreshing code")
                refreshCode()
            }

            is NewContainerEvent.ChangeLactaseIU -> {
                changeIUAmount(event.amount)
            }

            is NewContainerEvent.ChangePurchaseDate -> {
                changePurchaseDate(event.date)
            }

            is NewContainerEvent.ChangeExpiryDate -> {
                changeExpiryDate(event.date)
            }

            is NewContainerEvent.ChangePillsLeft -> {
                changePillsLeft(event.amount)
            }
        }
    }

    private fun refreshCode() {
        _state.update { currentState ->
            currentState.copy(code = generateCode())
        }
    }

    private fun changeIUAmount(newAmount: String) {
        val numericAmount = newAmount.toIntOrNull()
        if (numericAmount == null && newAmount.isNotEmpty()) return
        _state.update { currentState ->
            currentState.copy(lactaseIU = numericAmount)
        }
    }

    private fun changePillsLeft(newAmount: String) {
        val numericAmount = newAmount.toIntOrNull()
        if (numericAmount == null && newAmount.isNotEmpty()) return
        _state.update { currentState ->
            currentState.copy(pillsLeft = numericAmount)
        }
    }

    /**
     * If null is provided, the date isn't changed
     */
    private fun changePurchaseDate(newDate: Long) {
        _state.update { currentState ->
            currentState.copy(purchasedOn = newDate)
        }
    }

    private fun changeExpiryDate(newDate: Long) {
        _state.update { currentState ->
            currentState.copy(expiryDate = newDate)
        }
    }
}
