package me.mgaens.lactasebuddy.di

import me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container.NewContainerViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainKoinModule = module {

    viewModel { NewContainerViewModel() }
}