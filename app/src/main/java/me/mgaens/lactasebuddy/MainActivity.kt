package me.mgaens.lactasebuddy

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import me.mgaens.lactasebuddy.di.mainKoinModule
import me.mgaens.lactasebuddy.lactasecontainers.presentation.new_container.NewLactaseContainerScreen
import me.mgaens.lactasebuddy.ui.theme.LactaseBuddyTheme
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startKoin {
            androidLogger()
            androidContext(this@MainActivity.applicationContext)
            modules(mainKoinModule)
        }

        setContent {
            LactaseBuddyTheme {
                val navController = rememberNavController()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NewLactaseContainerScreen(navController = navController)
                }
            }
        }
    }
}
