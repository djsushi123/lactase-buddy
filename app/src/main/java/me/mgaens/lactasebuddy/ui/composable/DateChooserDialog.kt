package me.mgaens.lactasebuddy.ui.composable

import androidx.compose.material3.Button
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DateChooserDialog(
    show: Boolean,
    onDismiss: () -> Unit,
    onSave: (Long) -> Unit,
    initialDate: Long
) {
    if (!show) return

    val state = rememberDatePickerState(initialSelectedDateMillis = initialDate)

    DatePickerDialog(
        onDismissRequest = onDismiss,
        confirmButton = {
            Button(
                onClick = {
                    val selectedMillis = state.selectedDateMillis
                    if (selectedMillis != null) {
                        onSave(selectedMillis)
                    }
                }
            ) {
                Text(text = "Confirm")
            }
        }
    ) {
        DatePicker(state = state)
    }
}