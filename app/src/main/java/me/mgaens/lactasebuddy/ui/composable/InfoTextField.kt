package me.mgaens.lactasebuddy.ui.composable

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType

/**
 * A text field with an info button that can be pressed to open an [InfoDialog].
 *
 * @param isError if true, the text field will show an error that the value is required
 */
@Composable
fun InfoTextField(
    value: String,
    onValueChange: (String) -> Unit,
    labelText: String,
    modifier: Modifier = Modifier,
    onInfoOpen: () -> Unit,
    isError: Boolean = false
) {
    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        singleLine = true,
        trailingIcon = {
            IconButton(onClick = onInfoOpen) {
                Icon(
                    imageVector = Icons.Default.Info,
                    contentDescription = "Info"
                )
            }
        },
        isError = isError,
//        supportingText = {
//            AnimatedVisibility(visible = requiredError) {
//                Text(text = stringResource(R.string.required))
//            }
//        },
        label = { Text(text = labelText) },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        modifier = modifier
    )
}