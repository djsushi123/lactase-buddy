package me.mgaens.lactasebuddy.ui.composable

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import me.mgaens.lactasebuddy.R

@Composable
fun InfoDialog(show: Boolean, onDismiss: () -> Unit, text: String) {
    if (!show) return

    AlertDialog(
        onDismissRequest = onDismiss,
        confirmButton = {
            Button(onClick = onDismiss) {
                Text(text = stringResource(R.string.thanks_for_the_info))
            }
        },
        icon = {
            Icon(imageVector = Icons.Default.Info, contentDescription = "Info screen")
        },
        text = {
            Text(text = text)
        }
    )
}