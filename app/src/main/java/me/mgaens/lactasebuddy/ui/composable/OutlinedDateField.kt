package me.mgaens.lactasebuddy.ui.composable

import android.text.format.DateFormat
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.core.os.ConfigurationCompat
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

@Composable
@ReadOnlyComposable
private fun defaultLocale(): Locale {
    return ConfigurationCompat.getLocales(LocalConfiguration.current).get(0) ?: Locale.getDefault()
}

@Composable
fun OutlinedDateField(value: Long, onClick: () -> Unit, label: String, modifier: Modifier = Modifier) {
    val defaultLocale = defaultLocale()
    val displayedDate = remember(value, defaultLocale) {
        val pattern = DateFormat.getBestDateTimePattern(defaultLocale, "yyyyMMdd")
        val dateFormatter = SimpleDateFormat(pattern, defaultLocale)
        dateFormatter.timeZone = TimeZone.getDefault()
        val date = Date(value)
        dateFormatter.format(date)
    }

    OutlinedTextField(
        value = displayedDate,
        onValueChange = { },
        readOnly = true,
        singleLine = true,
        enabled = false,
        label = {
            Text(text = label)
        },
        colors = OutlinedTextFieldDefaults.colors(
            disabledTextColor = MaterialTheme.colorScheme.onSurface,
            disabledBorderColor = MaterialTheme.colorScheme.outline,
            disabledLabelColor = MaterialTheme.colorScheme.onSurfaceVariant
        ),
        modifier = modifier.clickable(onClick = onClick)
    )
}